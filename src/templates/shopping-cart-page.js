import React from 'react'
import PropTypes from 'prop-types'
import {graphql} from 'gatsby'
import ShoppingCartTemplate from '../components/ShoppingCartTemplate'
import Layout from '../components/Layout'

const ShoppingCartPage = ({data}) => {
  const {frontmatter} = data.markdownRemark
  return (
    <Layout>
      <ShoppingCartTemplate
        title={frontmatter.title}
       
        meta_title={frontmatter.meta_title}
        meta_description={frontmatter.meta_description}
      />
      <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    </Layout>
  )
}

ShoppingCartPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default ShoppingCartPage

export const shoppingcartPageQuery = graphql`
  query ShoppingCartPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      frontmatter {
        title
      
        meta_title
        meta_description
        heading
      }
    }
  }
`

import React from 'react'
import PropTypes from 'prop-types'
import {graphql} from 'gatsby'
import LandingPageTemplate from '../components/LandingPageTemplate'
import Layout from '../components/Layout'

const LandingPage = ({data}) => {
  const {frontmatter} = data.markdownRemark

  return (
    <Layout>
      <LandingPageTemplate
        title={frontmatter.title}
        meta_title={frontmatter.meta_title}
        meta_description={frontmatter.meta_description}
        heading={frontmatter.heading}
        description={frontmatter.description}
      />
      <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    </Layout>
  )
}

LandingPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default LandingPage

export const pageQuery = graphql`
  query LandingPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      frontmatter {
        title
        meta_title
        meta_description
        heading
        description
      }
    }
  }
`

import React from 'react'
import PropTypes from 'prop-types'
import {graphql} from 'gatsby'
import LogisticsPageTemplate from '../components/LogisticsPageTemplate'
import Layout from '../components/Layout'

const LogisticsPage = ({data}) => {
  const {frontmatter} = data.markdownRemark

  return (
    <Layout >
      <LogisticsPageTemplate
        title={frontmatter.title}
        meta_title={frontmatter.meta_title}
        meta_description={frontmatter.meta_description}
        heading={frontmatter.heading}
        description={frontmatter.description}
      />
      <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    </Layout>
  )
}

LogisticsPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default LogisticsPage

export const pageQuery = graphql`
  query LogisticsPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      frontmatter {
        title
        meta_title
        meta_description
        heading
        description
      }
    }
  }
`

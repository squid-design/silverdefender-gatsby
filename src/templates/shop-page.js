import React from 'react'
import PropTypes from 'prop-types'
import {graphql} from 'gatsby'
import ShopPageTemplate from '../components/ShopPageTemplate'
import Layout from '../components/Layout'

const ShopPage = ({data}) => {
  const {frontmatter} = data.markdownRemark
  return (
    <Layout>
      <ShopPageTemplate
        title={frontmatter.title}
       
        meta_title={frontmatter.meta_title}
        meta_description={frontmatter.meta_description}
      />
      <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    </Layout>
  )
}

ShopPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default ShopPage

export const shopPageQuery = graphql`
  query ShopPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      frontmatter {
        title
      
        meta_title
        meta_description
        heading
      }
    }
  }
`

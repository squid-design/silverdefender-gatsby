import React from 'react'
import PropTypes from 'prop-types'
import {graphql} from 'gatsby'
import CheckoutTemplate from '../components/CheckoutTemplate'
import Layout from '../components/Layout'

const CheckoutPage = ({data}) => {
  const {frontmatter} = data.markdownRemark
  return (
    <Layout>
      <CheckoutTemplate
        title={frontmatter.title}
       
        meta_title={frontmatter.meta_title}
        meta_description={frontmatter.meta_description}
      />
      <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    </Layout>
  )
}

CheckoutPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default CheckoutPage

export const checkoutPageQuery = graphql`
  query CheckoutPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      frontmatter {
        title
      
        meta_title
        meta_description
        heading
      }
    }
  }
`

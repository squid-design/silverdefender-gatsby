import React from 'react'
import Helmet from 'react-helmet'
import Offerings from '../Offerings'
import Testimonials from '../Testimonials'
import PropTypes from 'prop-types'
import posed from 'react-pose'
import { Link, graphql, StaticQuery } from 'gatsby'
import makeCarousel from 'react-reveal/makeCarousel'
// we'll need the Slide component for sliding animations
// but you can use any other effect
import Slide from 'react-reveal/Slide'
// we'll use styled components for this tutorial
// but you can use any other styling options ( like plain old css )
import styled, { css } from 'styled-components'
import LogisticsModal from '../LogisticsModal'

const Square = posed.div({
  hoverable: true,  
  pressable: true,
  init: {
    scale: 1,
    boxShadow: '0px 0px 0px rgba(0,0,0,0)'
  },
  hover: {
    scale: 1.2,
    boxShadow: '0px 5px 10px rgba(0,0,0,0.2)'
  },
  press: {
    scale: 1.1,
    boxShadow: '0px 2px 5px rgba(0,0,0,0.1)'
  }
});
const width = '300px', height = '300px';
const Container = styled.div`
  border: none;
  position: relative;
  margin: 0 auto;
  overflow: visible;
  width: ${width};
`;
const Children = styled.div`
  width: ${width};
  position: relative;
  height: ${height};
`;
const Arrow = styled.div`
  text-shadow: 1px 1px 1px #fff;
  z-index: 100;
  line-height: ${height};
  text-align: center;
  position: absolute;
  top: 0;
  width: 10%;
  font-size: 3em;
  cursor: pointer;
  user-select: none;
  ${props => props.right ? css`left: 115%;` : css`left: -25%;`}
`;
const Dot = styled.span`
  font-size: 1.5em;
  cursor: pointer;
  user-select: none;
`;
const Dots = styled.span`
  text-align: center;
  width: ${width};
  padding-top: 5%;
  display: block;
  position: relative;
  z-index: 100;
`;
const CarouselUI = ({ position, total, handleClick, children }) => (
  <Container>
    <Arrow onClick={handleClick} data-position={position - 1}>{'<'}</Arrow>
    <Children>
      {children}
    </Children>
    <Arrow right onClick={handleClick} data-position={position + 1}>{'>'}</Arrow>
    <Dots>
      {Array(...Array(total)).map((val, index) =>
        <Dot key={index} onClick={handleClick} data-position={index}>
          {index === position ? '● ' : '○ '}
        </Dot>
      )}
    </Dots>
  </Container>
);
const Carousel = makeCarousel(CarouselUI);

const LandingPageTemplate = ({
  title,
  heading,
  description,
  offerings,
  meta_title,
  meta_description,
  testimonials,
}) => (
    <div>
      <Helmet>
        <title>{meta_title}</title>
        <meta name='ANTIMICROBIAL STICKERS AND TAPES' content={meta_description} />
      </Helmet>
      <section className='hero hero-home is-bold is-medium'>

        <div className='hero-body'>
          <div className='container'>
            <div className='columns'>
              <div className='column is-10 is-offset-1'>
                <div className='section'>
                  <h1>99.9% Cleaner Surfaces. Powered by Silver.</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    
      <div className="homepage-container content-container">
      <hr />
        <section className='section z-form'>
          <div className='container'>
            <div className='columns'>
              <div className='column is-12'>
                <div className='columns is-gapless'>
                  <div className='column is-6 img1'>

                  </div>
                  <div className='column is-6'>
                    <p class="image-text">Protect Every Touch Point</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className='section z-form'>
          <div className='container'>
            <div className='columns'>
              <div className='column is-12'>
                <div className='columns is-gapless'>
                  <div className='column is-6 grid-text-desktop'>
                    <p class="image-text">Stretchable. Removable. Easy to Apply.</p>
                  </div>
                  <div className='column is-6 img2'>
                  </div>
                  <div className='column is-6 grid-text-mobile'>
                    <p class="image-text">Stretchable. Removable. Easy to Apply.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className='section z-form'>
          <div className='container'>
            <div className='columns'>
              <div className='column is-12'>
                <div className='columns'>
                  <div className='column is-6 img3'>

                  </div>
                  <div className='column is-6'>
                    <p class="image-text">Always Clean. Always On.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <hr />
        <section className='section section-howitworks'>
          <div className='columns'>
            <div className='column is-12'>
              <h1> How It Works</h1>
              <iframe src="https://www.youtube.com/embed/AZrAOKBLG-Q" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
            </div>
          </div>
        </section>
        <section className='section section--gradient'>
          <div className='columns'>
            <div className='column is-12'>
              <h1> Common Touch Points</h1>
            </div>
          </div>
          <div className="columns">
            <div className="column is-3">
              <Square className="grid-img1" />
            </div>
            <div className="column is-3">
              <Square className="grid-img1" />
            </div>
            <div className="column is-3">
              <Square className="grid-img1" />
            </div>
            <div className="column is-3">
              <Square className="grid-img1" />
            </div>
          </div>

        </section>
        <section className='section section--gradient'>
          <div className="container img-container">
            <div className='columns'>
              <div className='column is-12'>
                <div className='columns is-gapless'>
                  <div className='column is-6'>

                    <img className="img4" src="/../img/pixel-app.png" />
                  </div>

                  <div className='column is-6'>
                    <p class="image-text ">Simplified Ordering.</p>
                    <Link to="logistics"><button className="blue-button">View Your Order</button></Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
</div>
</div>
)
    

LandingPageTemplate.propTypes = {
        title: PropTypes.string,
  meta_title: PropTypes.string,
  meta_description: PropTypes.string,
  heading: PropTypes.string,
  description: PropTypes.string,
  offerings: PropTypes.shape({
        blurbs: PropTypes.array,
  }),
  testimonials: PropTypes.array,

}

export default LandingPageTemplate

import React from 'react';
import ReactDOM from 'react-dom';
import posed from 'react-pose';
import styled, { css } from 'styled-components';

const hoverContainerfourths = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Square1 = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    width: '49%',
    scale: 1,
    opacity: .5,
    zIndex: 1,
    boxShadow: '0px 0px 0px rgba(0,0,0,0)'
  },
  hover: {
    width: '85%',
    opacity: 1,
    zIndex: 9,
    boxShadow: '0px 5px 10px rgba(0,0,0,0.2)'
  },
  press: {
    boxShadow: '0px 2px 5px rgba(0,0,0,0.1)'
  }
});
const smallSquare1 = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 1,
    width: '10%',
    opacity: .5,
    zIndex: 1,
    boxShadow: '0px 0px 0px rgba(0,0,0,0)'
  },
  hover: {
    width:'85%',
    opacity: 1,
    zIndex: 9,
    boxShadow: '0px 5px 10px rgba(0,0,0,0.2)'
  },
  press: {
    boxShadow: '0px 2px 5px rgba(0,0,0,0.1)'
  }
});
const smallSquare2 = posed.div({
    hoverable: true,
    pressable: true,
    init: {
      scale: 1,
      width: '10%',
      opacity: .5,
      zIndex: 1,
      boxShadow: '0px 0px 0px rgba(0,0,0,0)'
    },
    hover: {
      width:'85%',
      opacity: 1,
      zIndex: 9,
      boxShadow: '0px 5px 10px rgba(0,0,0,0.2)'
    },
    press: {
      boxShadow: '0px 2px 5px rgba(0,0,0,0.1)'
    }
  });
  const smallSquare3 = posed.div({
    hoverable: true,
    pressable: true,
    init: {
      scale: 1,
      width: '10%',
      opacity: .5,
      zIndex: 1,
      boxShadow: '0px 0px 0px rgba(0,0,0,0)'
    },
    hover: {
      width:'85%',
      opacity: 1,
      zIndex: 9,
      boxShadow: '0px 5px 10px rgba(0,0,0,0.2)'
    },
    press: {
      boxShadow: '0px 2px 5px rgba(0,0,0,0.1)'
    }
  });
  const smallSquare4 = posed.div({
    hoverable: true,
    pressable: true,
    init: {
      scale: 1,
      width: '10%',
      opacity: .5,
      zIndex: 1,
      boxShadow: '0px 0px 0px rgba(0,0,0,0)'
    },
    hover: {
      width:'75%',
      opacity: 1,
      zIndex: 9,
      boxShadow: '0px 5px 10px rgba(0,0,0,0.2)'
    },
    press: {
      boxShadow: '0px 2px 5px rgba(0,0,0,0.1)'
    }
  });

const StyledSquare1 = styled(Square1)`
  width: 48%;
  background-image: url('/./img/doorhandle-left.png');
  background-size: cover;
  background-repeat: no-repeat;
  padding:0;
  min-height: 400px;
  position: absolute;
`;
const StyledSquare2 = styled(smallSquare2)`
width: 10%;
background-image: url('/./img/doorhandle-right.png');
background-size: cover;
background-repeat: no-repeat;
padding:0;
min-height: 400px;
position: absolute;
right: 35%;
`;
const StyledSquare3 = styled(smallSquare3)`
width: 10%;
background-image: url('/./img/doorhandle-right.png');
background-size: cover;
background-repeat: no-repeat;
padding:0;
min-height: 400px;
position: absolute;
right: 20%;
`;
const StyledSquare4 = styled(smallSquare4)`
width: 10%;
background-image: url('/./img/doorhandle-right.png');
background-size: cover;
background-repeat: no-repeat;
padding:0;
min-height: 400px;
position: absolute;
right: 3%;
`;

const Example2 = () => 
<hoverContainer>
  <div className="columns">
  <div className="column is-full">
        <StyledSquare1
        />
        <StyledSquare2 />
        <StyledSquare3 />
        <StyledSquare4 />
        </div>
        </div>
      </hoverContainer>;

export default Example2
import React from 'react'
import {Link, graphql, StaticQuery} from 'gatsby'
import SearchBox from '../SearchBox'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import Badge from 'react-bootstrap/Badge'

const NavBar = ({toggleNavbar, isActive}) => (
  <StaticQuery
    query={graphql`
            query SearchIndexQuery {
                siteSearchIndex {
                    index
                }
            }
        `}
    render={data => (
      <nav className='navbar' aria-label='main navigation'>
        <div className='navbar-brand'>
          <Link to='/' className='navbar-item'>
           <img src="/../img/SD-Logo-darksilver.png" />
          </Link>
        </div>
        <button
            className={`button navbar-burger ${isActive ? 'is-active' : ''}`}
            data-target='navMenu'
            onClick={toggleNavbar}
          >
            <span />
            <span />
            <span />
          </button>
        <div className={`navbar-menu ${isActive ? 'is-active' : ''}`} id='navMenu'>

          <div className='navbar-end'>
            <Link className='navbar-item' to='medical'>
                            Medical
            </Link>
            <Link className='navbar-item' to='commercial'>
                            Commercial
            </Link>
            <Link className='navbar-item' to='retail'>
                            Retail
            </Link>
            <Link className='navbar-item' to='shop'>
                            Shop  
            </Link>
            <Link className='navbar-item' to='cart'>
                            <FontAwesomeIcon icon={ faShoppingCart} /> <Badge pill variant="primary">
    0
  </Badge>
            </Link>
            <div className='navbar-item'>
              <div className='field is-grouped'>
                <div className='control'>
                  <Link
                    className='button'
                    to='/'>
                            Sign In
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav>
    )}
  />
)

export default NavBar

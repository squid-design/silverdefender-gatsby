import React, { Component } from 'react'
import { navigate } from 'gatsby-link'
import Helmet from 'react-helmet'
import {Button, Dropdown, DropdownButton, DropdownItem, DropdownMenu} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import PropTypes from 'prop-types'
import ModalExample from '../SeeAllModal'
import FilterModal from '../ShoppingFilterModal'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSortDown } from '@fortawesome/free-solid-svg-icons'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'

class ShopPageTemplate extends Component {
  constructor(props) {
    super(props)
    this.state = { isValidated: false }
  }



  render() {
    const { title, subtitle, meta_title, meta_description } = this.props
  
    return (
      <div>
        <Helmet>
          <title>{meta_title}</title>
          <meta name='description' content={meta_description} />
        </Helmet>
        <section className='hero is-primary is-bold is-medium'>
          <div className='hero-body'>
            <div className='container'>
              <div className='columns'>
                <div className='column is-vcentered is-10 is-offset-1'>
                  <div className='section'>
                    <h1 className='title'>
                      Shop By Your Business Type
                    </h1>
                    <div className="columns is-centered">
                    <div className="column is-half">
                    <Dropdown className="business-type-dropdown">
  <Dropdown.Toggle>
    <p>Business Type <FontAwesomeIcon icon={ faSortDown} /></p>
  </Dropdown.Toggle>

  <Dropdown.Menu>
    <Dropdown.Item href="#/action-1">School</Dropdown.Item>
    <Dropdown.Item href="#/action-2">Hospital</Dropdown.Item>
    <Dropdown.Item href="#/action-3">Home</Dropdown.Item>
  </Dropdown.Menu>
</Dropdown>
<ModalExample />
      
</div>
</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className='section'>
          <div className='container mt-5'>
            <div className="columns">
              <div className="column is-10 is-offset-1">
                <div className="columns">
                  <div className="column is-6 is-vcentered">
                  <h3> Featured Products</h3>
                    </div>
                    <div className="column is-3">
                      <div className="has-text-right"><FilterModal /></div>
                      </div>
                    <div className="column is-3">
                    <input style={{marginTop: '4%'}}className="input is-rounded is-vcentered" type="text" placeholder="Search Products" />
            
                      </div>
                      </div>
                
                </div>
                </div>
                
                </div>
                </section>
        <section className='section'>
          <div className='container mb-5'>
            <div className="columns">
              <div className="column is-10 is-offset-1">
                <div className="columns">
                  <div className="column is-3">
                    <div className="card">
                      <div className="card-content">
                        <div className="img3 product-image-here">

                        </div>
                      </div>
                      <footer className="card-footer">
                        <p className="card-footer-item">

                          [[product.title]]
    </p>
    <small>
                            [[product.price]]
      </small>
                        <button type="submit" className="button is-info">Add to Cart</button>
                      </footer>
                    </div>
                  </div>
                  <div className="column is-3">
                    <div className="card">
                      <div className="card-content">
                        <div className="img3 product-image-here">

                        </div>
                      </div>
                      <footer className="card-footer">
                      <p className="card-footer-item">

                        [[product.title]]
  </p>
  <small>
                          [[product.price]]
    </small>
                      <button type="submit" className="button is-info">Add to Cart</button>
                    </footer>
                    </div>
                  </div>
                  <div className="column is-3">
                    <div className="card">
                      <div className="card-content">
                        <div className="img3 product-image-here">

                        </div>
                      </div>
                      <footer className="card-footer">
                      <p className="card-footer-item">

                        [[product.title]]
  </p>
  <small>
                          [[product.price]]
    </small>
                      <button type="submit" className="button is-info">Add to Cart</button>
                    </footer>
                    </div>
                  </div>
                  <div className="column is-3">
                    <div className="card">
                      <div className="card-content">
                        <div className="img3 product-image-here">

                        </div>
                      </div>
                      <footer className="card-footer">
                      <p className="card-footer-item">

                        [[product.title]]
  </p>
  <small>
                          [[product.price]]
    </small>
                      <button type="submit" className="button is-info">Add to Cart</button>
                    </footer>
                    </div>
                  </div>
                  
                  
                </div>
                <div className="columns">
                <div className="column is-3">
                    <div className="card">
                      <div className="card-content">
                        <div className="img3 product-image-here">

                        </div>
                      </div>
                      <footer className="card-footer">
                      <p className="card-footer-item">

                        [[product.title]]
  </p>
  <small>
                          [[product.price]]
    </small>
                      <button type="submit" className="button is-info">Add to Cart</button>
                    </footer>
                    </div>
                  </div>
                  <div className="column is-3">
                    <div className="card">
                      <div className="card-content">
                        <div className="img3 product-image-here">

                        </div>
                      </div>
                      <footer className="card-footer">
                      <p className="card-footer-item">

                        [[product.title]]
  </p>
  <small>
                          [[product.price]]
    </small>
                      <button type="submit" className="button is-info">Add to Cart</button>
                    </footer>
                    </div>
                  </div>
                  <div className="column is-3">
                    <div className="card">
                      <div className="card-content">
                        <div className="img3 product-image-here">

                        </div>
                      </div>
                      <footer className="card-footer">
                      <p className="card-footer-item">

                        [[product.title]]
  </p>
  <small>
                          [[product.price]]
    </small>
                      <button type="submit" className="button is-info">Add to Cart</button>
                    </footer>
                    </div>
                  </div>
                  <div className="column is-3">
                    <div className="card">
                      <div className="card-content">
                        <div className="img3 product-image-here">

                        </div>
                      </div>
                      <footer className="card-footer">
                      <p className="card-footer-item">

                        [[product.title]]
  </p>
  <small>
                          [[product.price]]
    </small>
                      <button type="submit" className="button is-info">Add to Cart</button>
                    </footer>
                    </div>
                  </div>
                  </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
};

ShopPageTemplate.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  meta_title: PropTypes.string,
  meta_description: PropTypes.string,
}

export default ShopPageTemplate

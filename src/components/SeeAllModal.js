import React from 'react';
import { Modal } from 'react-bulma-components';

export default class ModalExample extends React.Component {
  state = { open: false };
  render() {
    return (
      <div>
         <p className="see-all-business"onClick={() => this.setState({ open: true })}>See All</p>
         <Modal show={this.state.open} onClose={() => this.setState({ open: false })}>
             <div className="modal-content">
                 <div className="columns is-centered">
                     <div className="column is-12">
                 <h4 className="has-text-centered"> All Business Options:</h4>
                 <ul className="has-text-centered">
                 <li>Schools</li>
<li>Daycare</li>
<li>Restaurants</li>
<li>Gyms</li>
<li>Hospitals</li>
<li>Cruise lines</li>
<li>Grocery Stores</li>
<li>Malls</li>
<li>Office Buildings</li>
</ul>
</div>
</div>
                 </div>

         </Modal>
      </div>
    )
  }
}
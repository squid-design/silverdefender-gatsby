import React, { Component } from 'react'
import { navigate } from 'gatsby-link'
import Helmet from 'react-helmet'
import {Button, Dropdown, DropdownButton, DropdownItem, DropdownMenu} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import PropTypes from 'prop-types'
import InfiniteCarousel from 'react-leaf-carousel'
import { Link, graphql, StaticQuery } from 'gatsby'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSortDown, faShoppingCart, faTimes } from '@fortawesome/free-solid-svg-icons'

class ShoppingCartTemplate extends Component {
  constructor(props) {
    super(props)
    this.state = { isValidated: false }
  }



  render() {
    const { title, subtitle, meta_title, meta_description } = this.props
    return (
      <div className="cart-background">
        <Helmet>
          <title>{meta_title}</title>
          <meta name='description' content={meta_description} />
        </Helmet>
        <div className="shopping-cart-container">
        <section className='section'>
          <div className='container '>
            <div className="columns">
              <div className="column is-12">
                <div className="mb-3">
              <Link to="/shop" ><small >Back to Shop</small></Link>
              </div>
                <div className="columns">
                  <div className="column is-6 is-vcentered">
                 
                  <h3> Shopping Cart</h3>
                    </div>
                      </div>
                      <div className="columns">
                        <div className="column is-12">
                        <table className="table is-striped">
                          <thead>
                            <tr>
                              <th></th>
                              <th>Item Name</th>
                              <th>Quantity</th>
                              <th>Price</th>
                                </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <th><figure class="image is-48x48">
  <img src="https://bulma.io/images/placeholders/128x128.png" />
</figure>
</th>
                                      <td>L Shape Door Handle
                                        </td>
                                        <td><input type="number" placeholder="1" name="quantity" min="1" max="999"/></td>
                                      <td>$10.00 USD</td>
                                      <td><FontAwesomeIcon icon={ faTimes} /></td>
                                      </tr>
                                      <tr>
                                    <th><figure class="image is-48x48">
  <img src="https://bulma.io/images/placeholders/128x128.png" />
</figure></th>
                                      <td>L Shape Door Handle
                                        </td>
                                        <td><input type="number" placeholder="1" name="quantity" min="1" max="999"/></td>
                                      <td>$10.00 USD</td>
                                      <td><FontAwesomeIcon icon={ faTimes} /></td>
                                      </tr>
                                      <tr>
                                    <th><figure class="image is-48x48">
  <img src="https://bulma.io/images/placeholders/128x128.png" />
</figure></th>
                                      <td>L Shape Door Handle
                                        </td>
                                        <td><input type="number" placeholder="1" name="quantity" min="1" max="999"/></td>
                                      <td>$10.00 USD</td>
                                      <td><FontAwesomeIcon icon={ faTimes} /></td>
                                      </tr>
                                      <tr>
                                        <td />
                                        <td />
                                        <td><b>Total:</b></td>
                                        <td>[INSERT TOTAL]</td>
                                        </tr>
                                  </tbody>
                          </table>
                         </div>
                        </div>
                   <div className="columns">
                     <div className="column is-6">
                        <Link to="/checkout" width="100%"className="button blue-button-checkout ">Proceed to Checkout</Link>
                        </div>
                        <div className="column is-6">
                        <Link to="/shop"  width="100%" className="button hollow-button-checkout">Continue Shopping</Link>
                </div>
                </div>
                </div>
                </div>
                </div>
                </section>
        <section className='section'>
          <div className='container mb-5'>
            <div className="columns">
              <div className="column is-12">
                <h4 className="pl-0"> Similar Products </h4>
                <InfiniteCarousel
    breakpoints={[
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
    ]}

    showSides={true}
    sidesOpacity={.5}
    sideSize={.1}
    slidesToScroll={3}
    slidesToShow={3}
    scrollOnDevice={true}
  >
    <div>
    <div className="card">
    <div className="card-content">
      <div className="img3 product-image-here">

      </div>
    </div>
    <footer className="card-footer">
      <p className="card-footer-item">

        [[product.title]]
</p>
<small>
          [[product.price]]
</small>
      <button type="submit" className="button is-info">Add to Cart</button>
    </footer>
  </div>
    </div>
    <div>
    <div className="card">
    <div className="card-content">
      <div className="img3 product-image-here">

      </div>
    </div>
    <footer className="card-footer">
      <p className="card-footer-item">

        [[product.title]]
</p>
<small>
          [[product.price]]
</small>
      <button type="submit" className="button is-info">Add to Cart</button>
    </footer>
  </div>
    </div>
    <div>
    <div className="card">
    <div className="card-content">
      <div className="img3 product-image-here">

      </div>
    </div>
    <footer className="card-footer">
      <p className="card-footer-item">

        [[product.title]]
</p>
<small>
          [[product.price]]
</small>
      <button type="submit" className="button is-info">Add to Cart</button>
    </footer>
  </div>
    </div>
    <div>
    <div className="card">
    <div className="card-content">
      <div className="img3 product-image-here">

      </div>
    </div>
    <footer className="card-footer">
      <p className="card-footer-item">

        [[product.title]]
</p>
<small>
          [[product.price]]
</small>
      <button type="submit" className="button is-info">Add to Cart</button>
    </footer>
  </div>
    </div>
    <div>
    <div className="card">
    <div className="card-content">
      <div className="img3 product-image-here">

      </div>
    </div>
    <footer className="card-footer">
      <p className="card-footer-item">

        [[product.title]]
</p>
<small>
          [[product.price]]
</small>
      <button type="submit" className="button is-info">Add to Cart</button>
    </footer>
  </div>
    </div>
    <div>
    <div className="card">
    <div className="card-content">
      <div className="img3 product-image-here">

      </div>
    </div>
    <footer className="card-footer">
      <p className="card-footer-item">

        [[product.title]]
</p>
<small>
          [[product.price]]
</small>
      <button type="submit" className="button is-info">Add to Cart</button>
    </footer>
  </div>
    </div>
    <div>
    <div className="card">
    <div className="card-content">
      <div className="img3 product-image-here">

      </div>
    </div>
    <footer className="card-footer">
      <p className="card-footer-item">

        [[product.title]]
</p>
<small>
          [[product.price]]
</small>
      <button type="submit" className="button is-info">Add to Cart</button>
    </footer>
  </div>
    </div>
    <div>
    <div className="card">
    <div className="card-content">
      <div className="img3 product-image-here">

      </div>
    </div>
    <footer className="card-footer">
      <p className="card-footer-item">

        [[product.title]]
</p>
<small>
          [[product.price]]
</small>
      <button type="submit" className="button is-info">Add to Cart</button>
    </footer>
  </div>
    </div>
    <div>
    <div className="card">
    <div className="card-content">
      <div className="img3 product-image-here">

      </div>
    </div>
    <footer className="card-footer">
      <p className="card-footer-item">

        [[product.title]]
</p>
<small>
          [[product.price]]
</small>
      <button type="submit" className="button is-info">Add to Cart</button>
    </footer>
  </div>
    </div>
    <div>
    <div className="card">
    <div className="card-content">
      <div className="img3 product-image-here">

      </div>
    </div>
    <footer className="card-footer">
      <p className="card-footer-item">

        [[product.title]]
</p>
<small>
          [[product.price]]
</small>
      <button type="submit" className="button is-info">Add to Cart</button>
    </footer>
  </div>
    </div>
    <div>
    <div className="card">
    <div className="card-content">
      <div className="img3 product-image-here">

      </div>
    </div>
    <footer className="card-footer">
      <p className="card-footer-item">

        [[product.title]]
</p>
<small>
          [[product.price]]
</small>
      <button type="submit" className="button is-info">Add to Cart</button>
    </footer>
  </div>
    </div>
    <div>
    <div className="card">
    <div className="card-content">
      <div className="img3 product-image-here">

      </div>
    </div>
    <footer className="card-footer">
      <p className="card-footer-item">

        [[product.title]]
</p>
<small>
          [[product.price]]
</small>
      <button type="submit" className="button is-info">Add to Cart</button>
    </footer>
  </div>
    </div>
    <div>
    <div className="card">
    <div className="card-content">
      <div className="img3 product-image-here">

      </div>
    </div>
    <footer className="card-footer">
      <p className="card-footer-item">

        [[product.title]]
</p>
<small>
          [[product.price]]
</small>
      <button type="submit" className="button is-info">Add to Cart</button>
    </footer>
  </div>
    </div>
    <div>
    <div className="card">
    <div className="card-content">
      <div className="img3 product-image-here">

      </div>
    </div>
    <footer className="card-footer">
      <p className="card-footer-item">

        [[product.title]]
</p>
<small>
          [[product.price]]
</small>
      <button type="submit" className="button is-info">Add to Cart</button>
    </footer>
  </div>
    </div>
    <div>
    <div className="card">
                      <div className="card-content">
                        <div className="img3 product-image-here">

                        </div>
                      </div>
                      <footer className="card-footer">
                        <p className="card-footer-item">

                          [[product.title]]
    </p>
    <small>
                            [[product.price]]
      </small>
                        <button type="submit" className="button is-info">Add to Cart</button>
                      </footer>
                    </div>
    </div>
    <div>
    <div className="card">
    <div className="card-content">
      <div className="img3 product-image-here">

      </div>
    </div>
    <footer className="card-footer">
      <p className="card-footer-item">

        [[product.title]]
</p>
<small>
          [[product.price]]
</small>
      <button type="submit" className="button is-info">Add to Cart</button>
    </footer>
  </div>
    </div>
  </InfiniteCarousel>
             </div>
             </div>
             </div>  

        </section>
        </div>
      </div>
    )
  }
};

ShoppingCartTemplate.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  meta_title: PropTypes.string,
  meta_description: PropTypes.string,
}

export default ShoppingCartTemplate

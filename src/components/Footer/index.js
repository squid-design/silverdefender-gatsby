import React from 'react'
import config from '../../../config'

const Footer = () => {
  return (
    <footer className='footer'>
      <div className='container'>
        <div className='columns'>
          <div className='column is-4 contact-us'>
          <form name="contact" method="POST" data-netlify="true">
            <h4>Contact Us</h4>
            <h5><b>Call us at (201) 921-2632</b></h5>
            <input class="input" type="email" placeholder="Your Email"></input>
            <textarea class="textarea" placeholder="Can we get more details about your products for our business?"></textarea>
          <input class="button is-info" type="submit" value="Submit"></input>
          </form>
            </div>
            <div className='column is-4'>
              <h5><b>Shop</b></h5>
              <ul>
              <li>Schools</li>
              <li>Daycare</li>
              <li>Restaurants</li>
              <li>Gyms</li>
              <li>Hospitals</li>
              <li>Cruise lines</li>
              <li>Grocery Stores</li>
              <li>Malls</li>
              <li>Office Buildings</li>
              <li>Find your business</li>
              <li>Shopping Help</li>

</ul>
            </div>
            <div className='column is-4'>
              <h5><b>Product Info</b></h5>
              <ul>
                <li>Instructions</li>
                <li>Use and care</li>
                <li>Regulations</li>
                <li>Testing Info</li>
                </ul>
                <h5>Silver Defender</h5>
                <ul>
                  <li>Our Values </li>
                  <li>Privacy and Security</li>
<li>Warranty</li>
<li>Terms of Use</li>
<li>Contact Us</li>
</ul>
            </div>
</div>
<hr />
<div class="columns">
<div className="column is-12">
              <small className="has-text-centered">Copyright © 2019 Silver Defender™</small>
            </div>
            </div>
      </div>
    </footer>
  )
}

export default Footer

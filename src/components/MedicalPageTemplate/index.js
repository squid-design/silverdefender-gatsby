import React from 'react'
import Content from '../Content'
import PropTypes from 'prop-types'
import makeCarousel from 'react-reveal/makeCarousel'
// we'll need the Slide component for sliding animations
// but you can use any other effect
import Slide from 'react-reveal/Slide'
import posed from 'react-pose'
import { Link, graphql, StaticQuery } from 'gatsby'
// we'll use styled components for this tutorial
// but you can use any other styling options ( like plain old css )
import styled, { css } from 'styled-components'

const Square = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 1,
    boxShadow: '0px 0px 0px rgba(0,0,0,0)'
  },
  hover: {
    scale: 1.2,
    boxShadow: '0px 5px 10px rgba(0,0,0,0.2)'
  },
  press: {
    scale: 1.1,
    boxShadow: '0px 2px 5px rgba(0,0,0,0.1)'
  }
});
const width = '300px', height = '300px';
const Container = styled.div`
  border: none;
  position: relative;
  margin: 0 auto;
  overflow: visible;
  width: ${width};
`;
const Children = styled.div`
  width: ${width};
  position: relative;
  height: ${height};
`;
const Arrow = styled.div`
  text-shadow: 1px 1px 1px #fff;
  z-index: 100;
  line-height: ${height};
  text-align: center;
  position: absolute;
  top: 0;
  width: 10%;
  font-size: 3em;
  cursor: pointer;
  user-select: none;
  ${props => props.right ? css`left: 115%;` : css`left: -25%;`}
`;
const Dot = styled.span`
  font-size: 1.5em;
  cursor: pointer;
  user-select: none;
`;
const Dots = styled.span`
  text-align: center;
  width: ${width};
  padding-top: 5%;
  display: block;
  position: relative;
  z-index: 100;
`;
const CarouselUI = ({ position, total, handleClick, children }) => (
  <Container>
    <Arrow onClick={handleClick} data-position={position - 1}>{'<'}</Arrow>
    <Children>
      {children}
    </Children>
    <Arrow right onClick={handleClick} data-position={position + 1}>{'>'}</Arrow>
    <Dots>
      {Array(...Array(total)).map((val, index) =>
        <Dot key={index} onClick={handleClick} data-position={index}>
          {index === position ? '● ' : '○ '}
        </Dot>
      )}
    </Dots>
  </Container>
);
const Carousel = makeCarousel(CarouselUI);

const MedicalPageTemplate = ({ title, content, contentComponent }) => {
  const PageContent = contentComponent || Content

  return (
    <div className="medical-container">
      <section className='hero hero-medical is-bold is-medium'>

        <div className='hero-body'>
          <div className='container'>
            <div className='columns'>
              <div className='column is-10 is-offset-1'>
                <div className='section'>
                  <h1>
                    Always Clean. Always On. 99.9% Cleaner Surfaces.
                  </h1>

                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="content-container">
        <section className='section z-form'>
          <div className='container'>
            <div className='columns'>
              <div className='column is-12'>
                <div className='columns is-gapless'>
                  <div className='column is-6 img1'>

                  </div>
                  <div className='column is-6'>
                    <p class="image-text">Antimicrobial Stickers and Tapes</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className='section z-form'>
          <div className='container'>
            <div className='columns'>
              <div className='column is-12'>
                <div className='columns is-gapless'>
                  <div className='column is-6 grid-text-desktop'>
                    <p class="image-text has-text-right">Protect Every Touchpoint</p>
                  </div>
                  <div className='column is-6 img2'>
                  </div>
                  <div className='column is-6 grid-text-mobile'>
                    <p class="image-text has-text-center">Protect Every Touchpoint</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className='section z-form'>
          <div className='container'>
            <div className='columns'>
              <div className='column is-12'>
                <div className='columns'>
                  <div className='column is-6 img3'>

                  </div>
                  <div className='column is-6'>
                    <p class="image-text has-text-left">Always Clean. Always On.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className='section section--gradient'>
          <div className='container'>
            <div className='columns'>
              <div className='column is-10 is-offset-1'>
                <section className='section '>
                  <div className='columns'>
                    <div className='column is-6'>
                      <p class="image-text has-text-right">Powered by Silver. Ion Exchange</p>
                    </div>
                    <div className='column has-text-justifty is-6'>
                      [Set of silver active icons]
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </section>
        <section className='section section--gradient'>
        <div className='columns'>
          <div className='column is-12'>
            <h3 className="h1-black"> Common Touch Points</h3>
          </div>
        </div>
        <div className="columns">
          <div className="column is-3">
            <Square className="grid-img1" />
          </div>
          <div className="column is-3">
            <Square className="grid-img2" />
          </div>
          <div className="column is-3">
            <Square className="grid-img3" />
          </div>
          <div className="column is-3">
            <Square className="grid-img4" />
          </div>
        </div>

      </section>

        <section className='section section-howitworks'>
          <div className="columns">
            <div className="column is-12">
              <h4 className="has-text-centered"> How It Works</h4>
              <iframe src="https://www.youtube.com/embed/AZrAOKBLG-Q" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>

            </div>
          </div>
        </section>


        <section className='section section--gradient'>
          <div className="container img-container">
            <div className='columns'>
              <div className='column is-12'>
                <div className='columns is-gapless'>
                  <div className='column is-6'>

                    <img className="img5" src="/../img/pixel-app.png" />
                  </div>

                  <div className='column is-6'>
                    <p class="image-text ">Simplified Ordering.</p>
                    <Link to="mailto:info@silverdefender.com"><button className="blue-button"><p>Get in Touch</p></button></Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  )
}

MedicalPageTemplate.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string,
  contentComponent: PropTypes.func,
}

export default MedicalPageTemplate

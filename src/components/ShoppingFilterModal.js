import React from 'react';
import { Modal } from 'react-bulma-components';

export default class FilterModal extends React.Component {
  state = { open: false };
  render() {
    return (
      <div>
         <p className="Filters"onClick={() => this.setState({ open: true })}><small>Filters</small></p>
         <Modal show={this.state.open} onClose={() => this.setState({ open: false })}>
             <div className="modal-content">
                 <div className="columns is-centered">
                     <div className="column is-12">
                 <h4 className="has-text-left"> Filters</h4>
                 <hr />
                 <h5>Categories</h5>
                 <ul className="has-text-left">
                 <li><label class="checkbox">
  <input type="checkbox" />
   Entrance doors 
</label></li>
<li><label class="checkbox">
  <input type="checkbox" />
   Interior doors
</label></li>
<li><label class="checkbox">
  <input type="checkbox" />
   Bathrooms
</label></li>
<li><label class="checkbox">
  <input type="checkbox" />
   Hospitals
</label></li>
<li><label class="checkbox">
  <input type="checkbox" />
   Offices
</label></li>
<li><label class="checkbox">
  <input type="checkbox" />
   Doctors Offices
</label></li>
<li><label class="checkbox">
  <input type="checkbox" />
   Fitness
</label></li>
</ul>
<h5>Sticker Shape</h5>
                 <ul className="has-text-left">
                 <li><label class="checkbox">
  <input type="checkbox" />
   Square
</label></li>
<li><label class="checkbox">
  <input type="checkbox" />
   Circular
</label></li>
<li><label class="checkbox">
  <input type="checkbox" />
   Rectangular
</label></li>
<li><label class="checkbox">
  <input type="checkbox" />
   Oval
</label></li>

</ul>
<button type="submit" class="btn btn-primary submit-button">Apply Filters </button>
</div>
</div>
                 </div>

         </Modal>
      </div>
    )
  }
}
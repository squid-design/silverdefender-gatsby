import React from 'react';
import { Modal } from 'react-bulma-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons'
import { faPlus } from '@fortawesome/free-solid-svg-icons'

export default class LogisticsModal extends React.Component {
  static defaultProps = {
    modal: {},
  }
  state = {
    open: false,
  };

  open = () => this.setState({ open: true });
  close = () => this.setState({ open: false });

  render() {
    return (
      <div>
        <button className="logistics-button blue-button" onClick={() => this.setState({ open: true })}>View Your Order</button>
        <Modal show={this.state.open} onClose={() => this.setState({ open: false })}>
          <div className="modal-content is-active">
            <div className="columns is-centered">
              <div className="column is-12">
                <h3 className="has-text-centered">VIEW YOUR ORDER</h3>
                <p className="has-text-centered">Ordering Silver Defender will always take you a couple clicks to protect your facilities.
Here’s how it works:</p>
              </div>
            </div>
            <div className="columns">
              <div className="column is-4 has-text-centered">
                <p className="is-flex">1.</p>
                <img className="mb-3" src="/../img/order-glyph1.png" />
                <p><small>Management
Sends Out an Order</small></p>
              </div>
              <div className="column is-4 has-text-centered">
                <p className="is-flex">2.</p>
                <img className="mb-3" src="/../img/order-glyph2.png" />
                <p><small>Each Location
    Customizes The Order</small></p>
              </div>
              <div className="column is-4 has-text-centered">
                <p className="is-flex">3.</p>
                <img className="mb-4" src="/../img/order-glyph3.png" />
                <p><small>Stickers are Shipped
    to Each Location</small></p>
              </div>
            </div>
            <div className="columns">
              <div className="column is-12">
                <h4 className="has-text-left mt-4">MY ORDERS:</h4>
                <table className="table order-table is-striped">
                  <thead>
                    <tr>
                      <th>Quantity</th>
                      <th>Touchpoints</th>
                      <th>Total</th>
                      <th><abbr title="Edit">Edit</abbr></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th>3 packs</th>
                      <td>
                        <img src="/../img/glyph_doorknob.png" />
                        <img src="/../img/glyph_escalator.png" />
                        <img src="/../img/glyph_shoppingcart.png" />

                      </td>
                      <td> $100 USD </td>
                      <td><FontAwesomeIcon icon={faPencilAlt} /></td>
                    </tr>
                    <tr>
                      <th>9 packs</th>
                      <td>
                        <img src="/../img/glyph_doorknob.png" />
                        <img src="/../img/glyph_escalator.png" />
                      </td>
                      <td> $500 USD </td>
                      <td><FontAwesomeIcon icon={faPencilAlt} /></td>
                    </tr>
                    <tr>
                      <th>15 packs</th>
                      <td>
                        <img src="/../img/glyph_shoppingcart.png" />
                        <img src="/../img/glyph_cell.png" />
                        <img src="/../img/glyph_faucet.png" />
                      </td>
                      <td> $1,000 USD </td>
                      <td><FontAwesomeIcon icon={faPencilAlt} /></td>
                    </tr>
                    <tr>
                      <th>10 packs</th>
                      <td>
                        <img src="/../img/glyph_doorknob.png" />
                        <img src="/../img/glyph_faucet.png" />
                      </td>
                      <td> $700 USD </td>
                      <td><FontAwesomeIcon icon={faPencilAlt} /></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="columns">
              <div className="column is-12">
                <h4 className="has-text-left mt-4">ADD MARKETING:</h4>
              </div>
            </div>
            <div className="columns blue-background">
              <div className="column has-text-centered is-4">
                <img src="/../img/product-image.png" />
              </div>
              <div className="column mt-5 is-8">
                <h5> <label class="checkbox">
                  <input className="checkbox-box" type="checkbox" />
                  ADD MARKETING FOR $10 PER LOCATION
</label>
                </h5>
                <small>Some fine print explaining how adding marketing works, an example, and how it is billed.</small>
              </div>
            </div>
            <div className="columns">
              <div className="column is-12">
                <h4 className="has-text-left mt-4">WHO WOULD YOU LIKE TO SEND THIS ORDER TO?</h4>
              </div>
            </div>
            <div className="columns">
              <div className="column is-4">
                <button className="hollow-button-blue">DELEGATE</button>
              </div>
              <div className="column is-4">
                <button className="hollow-button-blue">ADD LIST</button>
              </div>
            </div>
            <div className="columns">
              <div className="column is-8">
                <div className="field">
                  <label>EMAIL:</label>
                  <p className="control has-icons-right">

                    <input className="input" type="email" placeholder="Email" />
                    <span className="icon is-small is-right">
                      <FontAwesomeIcon icon={faPlus} />
                    </span>
                  </p>
                </div>
                <div class="control">
                  <label>YOUR MESSAGE:</label>
                  <textarea className="textarea" placeholder="Type any specific instructions you’d like your facilities to know before they place their order."></textarea>
                </div>
                <button width="100%" className="mt-5 send-order-button">EMAIL ORDER</button>
              </div>
            </div>
          </div>

        </Modal>
      </div>
    )
  }
}
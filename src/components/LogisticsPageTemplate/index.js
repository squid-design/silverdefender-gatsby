import React, { Component } from 'react'
import { navigate } from 'gatsby-link'
import Helmet from 'react-helmet'
import {Button, Dropdown, DropdownButton, DropdownItem, DropdownMenu} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import PropTypes from 'prop-types'
import Tabs from '../Tabs'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'

class LogisticsPageTemplate extends Component {
  constructor(props) {
    super(props)
    this.state = { isValidated: false }
  }
  onChangeHandler=event=>{
    
        console.log(event.target.files[0])
    
    }

 onChangeHandler=event=>{
    this.setState({
      selectedFile: event.target.files[0],
      loaded: 0,
    })
  }
  render() {
    const { title, subtitle, meta_title, meta_description } = this.props
  
    return (
      <div className="logistics-container">
        <Helmet bodyAttributes={{style: 'background-color : #f3f3f3'}}>
          <title>{meta_title}</title>
          <meta name='description' content={meta_description} />
        </Helmet>

            <div className='container mt-5'>
              <div className='columns'>
                <div className='column is-vcentered is-10 is-offset-1'>
                  <div className="columns">
                    <div className="column is-7">
                    <h1>
                      View Your Order
                    </h1>
                    </div>
                    <div className="column is-5  is-offset-1 is-flex">
                    <figure class="image is-64x64">
  <img class="is-rounded" src="https://bulma.io/images/placeholders/128x128.png" />
</figure>
<div className="ml-4">
<p className="mb-0"> Your Product Specialist</p>
<small><FontAwesomeIcon icon={faPhoneAlt} /> 555-555-5555</small><br/>
  <small><FontAwesomeIcon icon={faEnvelope} /> specialist@gmail.com </small>
  </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
   
        <section className='section'>
          <div className='container card mt-5'>
            <div className="columns card-content">
              <div className="column is-12">
               
                  <h3>Your Estimate</h3>
                  <p className="pl-0 pt-2 pb-2"><small>This estimate is an example reflecting needs based on your specific location. To edit your order, choose one of the options below. You can customize on a per brand basis, or customize each location individually.</small></p>
                 <hr />
                 <table className="table order-table is-striped">
                  <thead>
                    <tr>
                      <th>Quantity</th>
                      <th>Touchpoints</th>
                      <th>Total</th>
                   </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th>3 packs</th>
                      <td>
                        <img src="/../img/glyph_doorknob.png" />
                        <img src="/../img/glyph_escalator.png" />
                        <img src="/../img/glyph_shoppingcart.png" />

                      </td>
                      <td> $100 USD </td>
                  </tr>
                    <tr>
                      <th>9 packs</th>
                      <td>
                        <img src="/../img/glyph_doorknob.png" />
                        <img src="/../img/glyph_escalator.png" />
                      </td>
                      <td> $500 USD </td>
                    </tr>
                    <tr>
                      <th>15 packs</th>
                      <td>
                        <img src="/../img/glyph_shoppingcart.png" />
                        <img src="/../img/glyph_cell.png" />
                        <img src="/../img/glyph_faucet.png" />
                      </td>
                      <td> $1,000 USD </td>
                    </tr>
                    <tr>
                      <th>10 packs</th>
                      <td>
                        <img src="/../img/glyph_doorknob.png" />
                        <img src="/../img/glyph_faucet.png" />
                      </td>
                      <td> $700 USD </td>
                   </tr>
                   <tr>
                     <th></th>
                     <td></td>
                     <td><b>$2,300 USD</b></td>
                     </tr>
                  </tbody>
                </table>
                <div className="columns is-right">
                 <div className="column is-4">
                   <button className="blue-button-full">CUSTOMIZE GLOBAL</button>
                 </div>
                 <div className="column is-4">
                   <button className="blue-button-full">CUSTOMIZE LOCATION</button>
                 </div>
               </div>
                    </div>
                 
                      </div>
                
                </div>
                </section>
                <section className='section'>
          <div className='container card mt-5 mb-5'>
            <div className="columns card-content">
              <div className="column is-12">
               
                  <h3>Invite Others to Customize This Order</h3>
                  <p className="pl-0 pt-2 pb-2"><small>Delegate individuals to customize orders for their own locations, or send orders directly to a bulk list by using our secure website (no App necessary). You'll have another opportunity to invite others if you choose to customize your order by location above first.</small></p>
                 <hr />
                 <div className="columns">
                   <div className="column is-12">
                   <Tabs>
        <div label="DELEGATE">
        <div className="columns">
              <div className="column is-6">
                <div className="field">
                  <label>EMAIL:</label>
                  <div className="control has-icons-right">
                    <input className="input" type="email" placeholder="Email" />
                    <span className="icon is-small is-right">
                      <FontAwesomeIcon icon={faPlus} />
                    </span>
                  </div>
                </div>
                <div class="control">
                  <label>YOUR MESSAGE:</label>
                  <textarea className="textarea" placeholder="Type any specific instructions you’d like your facilities to know before they place their order."></textarea>
                </div>
                <button  className="mt-5 send-order-button">EMAIL ORDER</button>
              </div>
            </div>
        </div>
        <div label="ADD LIST">
        <div className="columns">
              <div className="column is-6">
          <div className="file-upload">
        <input className="has-text-centered pt-5 pr-5 pl-5"type="file" name="file" placeholder="Add emails here"onChange={this.onChangeHandler}/>
        <br />
          <small className="pl-5">Supports .CSV and .XCL file formats</small>
          </div>
          <button className="send-order-button mt-5">EMAIL ORDER</button>
        </div>
        </div>
        </div>
    
      </Tabs>
               </div>
               </div>  
               </div>
               </div>
               </div>
                </section>

        
      </div>
    )
  }
};

LogisticsPageTemplate.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  meta_title: PropTypes.string,
  meta_description: PropTypes.string,
}

export default LogisticsPageTemplate

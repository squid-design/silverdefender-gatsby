import React, { Component } from 'react'
import { navigate } from 'gatsby-link'
import Helmet from 'react-helmet'
import {Button, Dropdown, DropdownButton, DropdownItem, DropdownMenu} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import PropTypes from 'prop-types'
import InfiniteCarousel from 'react-leaf-carousel'
import { Link, graphql, StaticQuery } from 'gatsby'
import {Form } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSortDown } from '@fortawesome/free-solid-svg-icons'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'

class CheckoutTemplate extends Component {
  constructor(props) {
    super(props)
    this.state = { isValidated: false }
  }



  render() {
    const { title, subtitle, meta_title, meta_description } = this.props
    return (
      <div className="cart-background">
        <Helmet>
          <title>{meta_title}</title>
          <meta name='description' content={meta_description} />
        </Helmet>
        <div className="shopping-cart-container">
        <section className='section'>
          <div className='container '>
            <div className="columns">
              <div className="column is-12">
                <div className="mb-3">
              <Link to="/cart" ><small >Back to Shopping Cart</small></Link>
              </div>
             
                  <h3> Checkout</h3>
                  <h5>Your Order Summary</h5>
                  <table className="table is-striped">
                          <thead>
                            <tr>
                              <th></th>
                              <th>Item Name</th>
                              <th>Quantity</th>
                              <th>Price</th>
                                </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <th><figure class="image is-48x48">
  <img src="https://bulma.io/images/placeholders/128x128.png" />
</figure>
</th>
                                      <td>L Shape Door Handle
                                        </td>
                                        <td><input type="number" placeholder="1" name="quantity" min="1" max="999"/></td>
                                      <td>$10.00 USD</td>
                                      </tr>
                                      <tr>
                                    <th><figure class="image is-48x48">
  <img src="https://bulma.io/images/placeholders/128x128.png" />
</figure></th>
                                      <td>L Shape Door Handle
                                        </td>
                                        <td><input type="number" placeholder="1" name="quantity" min="1" max="999"/></td>
                                      <td>$10.00 USD</td>
                                      </tr>
                                      <tr>
                                    <th><figure class="image is-48x48">
  <img src="https://bulma.io/images/placeholders/128x128.png" />
</figure></th>
                                      <td>L Shape Door Handle
                                        </td>
                                        <td><input type="number" placeholder="1" name="quantity" min="1" max="999"/></td>
                                      <td>$10.00 USD</td>
                                      </tr>
                                      <tr>
                                        <td />
                                        <td />
                                        <td><b>Total:</b></td>
                                        <td>[INSERT TOTAL]</td>
                                        </tr>
                                  </tbody>
                          </table>
                             <div className="columns">
                            <div className="column is-half">
                            <h5> Shipping Information</h5>
                            <Form>
                            <Form.Group controlId="exampleForm.ControlInput1">
                              <Form.Label>Name</Form.Label>
                              <Form.Control type="name" placeholder="Your Name" />
                            </Form.Group>
                            <Form.Group controlId="exampleForm.ControlInput1">
                              <Form.Label>Address</Form.Label>
                              <Form.Control type="text" placeholder="Address Line 1" />
                            </Form.Group>
                            <Form.Group controlId="exampleForm.ControlInput1">
                              <Form.Control type="text" placeholder="Address Line 2 (optional)" />
                            </Form.Group>
                            <Form.Group controlId="exampleForm.ControlInput1">
                              <Form.Label>City</Form.Label>
                              <Form.Control type="text" placeholder="City" />
                            </Form.Group>
                            <Form.Group controlId="exampleForm.ControlSelect1">
                              <Form.Label>State</Form.Label>
                              <Form.Control as="select">
                                <option>United States</option>
                                <option>[NEED STATE DATA HERE]</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                              </Form.Control>
                            </Form.Group>
                            <Form.Group controlId="exampleForm.ControlInput1">
                              <Form.Label>Zip Code</Form.Label>
                              <Form.Control type="text" placeholder="Zip Code" />
                            </Form.Group>
                            </Form>
                            <label class="checkbox">
  <input type="checkbox" />
    Billing address is the same as shipping
</label>
                            </div>
                            </div>
                            <div className="columns">
                            <div className="column is-half">
                            <h5> Contact Information</h5>
                            <Form>
                            <Form.Group controlId="exampleForm.ControlInput1">
                              <Form.Label>Name</Form.Label>
                              <Form.Control type="name" placeholder="Your Name" />
                            </Form.Group>
                            <Form.Group controlId="exampleForm.ControlInput1">
                              <Form.Label>Email</Form.Label>
                              <Form.Control type="text" placeholder="example@gmail.com" />
                            </Form.Group>
                            <Form.Group controlId="exampleForm.ControlInput1">
                              <Form.Label>Company Name</Form.Label>
                              <Form.Control type="text" placeholder="Company" />
                            </Form.Group>
                           
                            <Form.Group controlId="exampleForm.ControlInput1">
                              <Form.Label>Phone Number</Form.Label>
                              <Form.Control type="text" placeholder="xxx-xxx-xxxx" />
                            </Form.Group>
                            </Form>
                            </div>
                            </div>
                            <div className="columns">
                            <div className="column is-half">
                            <h5>Payment</h5>
                            <Form>
                            <Form.Group controlId="exampleForm.ControlInput1">
                              <Form.Label>Credit Card Number</Form.Label>
                              <Form.Control type="name" placeholder="xxxxxxxxxxx" />
                            </Form.Group>
                            <Link to="/checkout-complete" width="100%"className="button blue-button-checkout ">Submit Order</Link>
                       
                            </Form>
                           
                            </div>
                            </div>
                            </div>
                            </div>
                          </div>
                        
                          
                          
                          

                          
               
      
             
                </section>

       
        </div>
      </div>
    )
  }
};

CheckoutTemplate.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  meta_title: PropTypes.string,
  meta_description: PropTypes.string,
}

export default CheckoutTemplate

import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import posed from 'react-pose'
import styled, { css } from 'styled-components'



const hoverContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Square = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    width: '45%',
    scale: 1,
    opacity: .5,
    zIndex: 1,
    boxShadow: '0px 0px 0px rgba(0,0,0,0)'
  },
  hover: {
    width: '75%',
    opacity: 1,
    zIndex: 9,
    boxShadow: '0px 5px 10px rgba(0,0,0,0.2)'
  },
  press: {
    boxShadow: '0px 2px 5px rgba(0,0,0,0.1)'
  }
});
const Square2 = posed.div({
  hoverable: true,
  pressable: true,
  init: {
    scale: 1,
    width: '45%',
    opacity: .5,
    zIndex: 1,
    boxShadow: '0px 0px 0px rgba(0,0,0,0)'
  },
  hover: {
    width:'75%',
    opacity: 1,
    zIndex: 9,
    boxShadow: '0px 5px 10px rgba(0,0,0,0.2)'
  },
  press: {
    boxShadow: '0px 2px 5px rgba(0,0,0,0.1)'
  }
});

const StyledSquare = styled(Square)`

`;
const StyledSquare2 = styled(Square2)`

`;

class Example extends Component {
  constructor (props) {
    super(props)

    this.state = {
      whichHovered: null,
    }
  }

  rightSquareHover () {
    this.setState({
      whichHovered: 'right',
    })
  }

  hoverEnd () {
    this.setState({
      whichHovered: null,
    })
  }

  leftSquareHover () {
    this.setState({
      whichHovered: 'left',
    })
  }

  render () {
    const leftSquareClasses = 'elbowdoor-pic' + (this.state.whichHovered === 'left' ? ' large' : '') + (this.state.whichHovered === 'right' ? ' small' : '')
    const rightSquareClasses = 'handdoor-pic' + (this.state.whichHovered === 'left' ? ' small' : '') + (this.state.whichHovered === 'right' ? ' large' : '')
    return <hoverContainer>
      <div className='columns'>
        <div className='column is-full'>
          <StyledSquare className={leftSquareClasses} onMouseEnter={() => this.leftSquareHover()} onMouseLeave={() => this.hoverEnd()} />
          <StyledSquare2 className={rightSquareClasses} onMouseEnter={() => this.rightSquareHover()} onMouseLeave={() => this.hoverEnd()} />
        </div>
      </div>
    </hoverContainer>
  }
}

export default Example
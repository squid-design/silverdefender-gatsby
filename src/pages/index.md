---
templateKey: 'home-page'
title: Silver Defender
meta_title: Home | Silver Defender
meta_description: >-
  Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
  ridiculus mus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam
  venenatis vestibulum. Sed posuere consectetur est at lobortis. Cras mattis
  consectetur purus sit amet fermentum.
heading: Lorem ipsum dolor sit amet
description: >-
  Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur,
  adipisci velit...
---
{{content}}
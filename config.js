module.exports = {
  siteTitle: 'Silver Defender', // Site title.
  siteTitleAlt: 'Microbial wraps to protect all surfaces powered by silver technology', // Alternative site title for SEO.
  siteLogo: '/icons/icon-512x512.png', // Logo used for SEO and manifest.
  siteUrl: 'https://www.silverdefender.com', // Domain of your website without pathPrefix.
  pathPrefix: '/', // Prefixes all links. For cases when deployed to example.github.io/gatsby-starter-business/.
  siteDescription: 'Microbial wraps to protect all surfaces powered by silver technology', // Website description used for RSS feeds/meta description tag.
  siteRss: '/rss.xml',
  siteFBAppID: '', // FB Application ID for using app insights
  googleTagManagerID: '', // GTM tracking ID.
  disqusShortname: 'gatsby-business-starter', // Disqus shortname.
  userName: 'Aislynn',
  userTwitter: '',
  userLocation: 'New Jersy, USA',
  userDescription: '',
  copyright: 'Copyright © Silver Defender 2019 Patent Pending All Rights Reserved.', // Copyright string for the footer of the website and RSS feed.
  themeColor: '#00d1b2', // Used for setting manifest and progress theme colors.
  backgroundColor: '#ffffff', // Used for setting manifest background color.
}
